using System.Collections.Generic;
using Xunit;
using Moq;

namespace TurtleRace
{
    public class GameTests
    {
        [Fact]
        public void GameIsOver_TurtleIsOutsideTableX_GameEnds()
        {
            var turtleMock = new Mock<Turtle>();
            var turtles = new List<Turtle>();

            turtleMock.SetupGet(x => x.XPosition).Returns(10);
            turtleMock.SetupGet(x => x.YPosition).Returns(9);

            turtles.Add(turtleMock.Object);

            var table = new Table(10, 10);

            var game = new Game(table, turtles);

            Assert.True(game.IsGameOver());

        }

        [Fact]
        public void GameIsOver_TurtleIsOutsideTableY_GameEnds()
        {
            var turtleMock = new Mock<Turtle>();
            var turtles = new List<Turtle>();

            turtleMock.SetupGet(x => x.XPosition).Returns(9);
            turtleMock.SetupGet(x => x.YPosition).Returns(10);

            turtles.Add(turtleMock.Object);

            var table = new Table(10, 10);

            var game = new Game(table, turtles);

            Assert.True(game.IsGameOver());

        }

        [Fact]
        public void GameIsOver_TurtleIsInsideTable_GameDoesNotEnd()
        {
            var turtleMock = new Mock<Turtle>();
            var turtles = new List<Turtle>();

            turtleMock.SetupGet(x => x.XPosition).Returns(9);
            turtleMock.SetupGet(x => x.YPosition).Returns(9);

            turtles.Add(turtleMock.Object);

            var table = new Table(10, 10);

            var game = new Game(table, turtles);

            Assert.False(game.IsGameOver());
        }

        [Fact]
        public void GameStart_GameStartSingleTurtle_SingleTurtleHasPositionsInitialized()
        {
            var turtle = new Turtle();
            var turtles = new List<Turtle>();

            turtles.Add(turtle);

            var table = new Table(10, 10);

            var game = new Game(table, turtles);

            game.GameStart();

            Assert.Equal(5, turtle.XPosition);
            Assert.Equal(5, turtle.YPosition);

            /*
            insert obvious joke about all the single turtles here
            */
        }

        [Fact]
        public void GameStart_GameStartWithMultipleTurtles_TurtlesPositionsInitialized()
        {
            var turtle1 = new Turtle();
            var turtle2 = new Turtle();
            var turtles = new List<Turtle>();

            turtles.Add(turtle1);
            turtles.Add(turtle2);

            var table = new Table(10, 10);

            var game = new Game(table, turtles);

            game.GameStart();

            Assert.Equal(4, turtle1.XPosition);
            Assert.Equal(5, turtle1.YPosition);

            Assert.Equal(5, turtle2.XPosition);
            Assert.Equal(5, turtle2.YPosition);
        }

        [Fact]
        public void DoTic_DoTicCalled_TurtlesMove()
        {
            var turtleMock1 = new Mock<Turtle>();
            var turtleMock2 = new Mock<Turtle>();
            var turtles = new List<Turtle>();

            turtles.Add(turtleMock1.Object);
            turtles.Add(turtleMock2.Object);

            /*turtleMock1.Setup(x => x.Move());
            turtleMock2.Setup(x => x.Move());*/

            var table = new Table(10, 10);

            var game = new Game(table, turtles);

            game.DoTic();

            turtleMock1.Verify(x => x.Move(), Times.Exactly(1));
            turtleMock2.Verify(x => x.Move(), Times.Exactly(1));
        }
    }
}