using Xunit;
using Moq;


namespace TurtleRace
{
    public class GameRunnerTests
    {
        [Fact]
        public void Play_CallPlayGame_LoopsUntilGameEnds()
        {
            var gameMock = new Mock<Game>();

            gameMock.SetupSequence(x => x.IsGameOver()).Returns(false).Returns(false).Returns(true);

            var gameRunner = new GameRunner(gameMock.Object);

            gameRunner.Play();

            gameMock.Verify(x => x.IsGameOver(), Times.Exactly(3));
        }

        [Fact]
        public void Play_CallPlayGame_GameStartIsCalled()
        {
            var gameMock = new Mock<Game>();

            var gameRunner = new GameRunner(gameMock.Object);

            gameMock.Setup(x => x.IsGameOver()).Returns(true);

            gameRunner.Play();

            gameMock.Verify(x => x.GameStart(), Times.Exactly(1));
        }

        [Fact]
        public void Play_CallPlayGame_GameEndIsCalled()
        {
            var gameMock = new Mock<Game>();

            var gameRunner = new GameRunner(gameMock.Object);

            gameMock.Setup(x => x.IsGameOver()).Returns(true);

            gameRunner.Play();

            gameMock.Verify(x => x.GameEnd(), Times.Exactly(1));
        }

        [Fact]
        public void Play_CallPlayGame_TicEachLoop()
        {
            var gameMock = new Mock<Game>();

            gameMock.SetupSequence(x => x.IsGameOver()).Returns(false).Returns(false).Returns(true);

            var gameRunner = new GameRunner(gameMock.Object);

            gameRunner.Play();

            gameMock.Verify(x => x.DoTic(), Times.Exactly(2));
        }
    }
}

