using Xunit;
using Moq;
using System.Collections.Generic;
using System;

namespace TurtleRace
{
    public class TurtleTests
    {
        [Fact]
        public void Move_MoveCalledWithEmptyList_PositionsChange()
        {
            var randomMock = new Mock<Random>();
            randomMock.SetupSequence(x => x.Next()).Returns(0);


            var turtle = new Turtle(new XYCoordinates(5, 5), randomMock.Object);

            var turtles = new List<Turtle>();

            turtle.Move(turtles);

            Assert.Equal(6, turtle.YPosition);

        }


        [Fact]
        public void Move_MoveCalledWithBlockingTurtle_PositionsChangeAndDoNotOverlap()
        {
            var randomMock = new Mock<Random>();
            randomMock.SetupSequence(x => x.Next()).Returns(0);


            var turtle1 = new Turtle(new XYCoordinates(5, 5), randomMock.Object);
            var turtle2 = new Turtle(new XYCoordinates(5, 6), randomMock.Object);

            var turtles = new List<Turtle>();
            turtles.Add(turtle1);
            turtles.Add(turtle2);

            turtle1.Move(turtles);

            Assert.Equal(6, turtle1.XPosition);
        }
    }
}