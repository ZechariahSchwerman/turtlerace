using Xunit;
using Moq;

namespace TurtleRace
{
    public class XYCoordinatesTests
    {
        [Fact]
        public void Change_ChangeDirectionNorth_YPositionIncremented()
        {
            var xyCoordinates = new XYCoordinates(5, 5);

            xyCoordinates = XYCoordinates.NewCoordsChanged(xyCoordinates, Direction.North);

            Assert.Equal(6, xyCoordinates.YCoordinate);
            Assert.Equal(5, xyCoordinates.XCoordinate);
        }

        [Fact]
        public void Change_ChangeDirectionSouth_YPositionDecremented()
        {
            var xyCoordinates = new XYCoordinates(5, 5);

            xyCoordinates = XYCoordinates.NewCoordsChanged(xyCoordinates, Direction.South);

            Assert.Equal(4, xyCoordinates.YCoordinate);
            Assert.Equal(5, xyCoordinates.XCoordinate);
        }

        [Fact]
        public void Change_ChangeDirectionEast_XPositionIncremented()
        {
            var xyCoordinates = new XYCoordinates(5, 5);

            xyCoordinates = XYCoordinates.NewCoordsChanged(xyCoordinates, Direction.East);

            Assert.Equal(5, xyCoordinates.YCoordinate);
            Assert.Equal(6, xyCoordinates.XCoordinate);
        }

        [Fact]
        public void Change_ChangeDirectionWest_XPositionDecremented()
        {
            var xyCoordinates = new XYCoordinates(5, 5);

            xyCoordinates = XYCoordinates.NewCoordsChanged(xyCoordinates, Direction.West);

            Assert.Equal(5, xyCoordinates.YCoordinate);
            Assert.Equal(4, xyCoordinates.XCoordinate);
        }
    }
}