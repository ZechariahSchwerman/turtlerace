using System;
using System.Collections.Generic;


namespace TurtleRace
{

    public class Game
    {
        readonly private Table _table;
        readonly private IList<Turtle> _turtles;

        public Game()
        {

        }

        public Game(Table table, IList<Turtle> turtles)
        {

            _table = table;

            _turtles = turtles;

        }

        virtual public bool IsGameOver()
        {
            foreach (Turtle turtle in _turtles)
            {
                if (turtle.XPosition >= _table.XLength || turtle.YPosition >= _table.YLength)
                {
                    return true;
                }
            }

            return false;
        }

        virtual public void GameStart()
        {
            for (int i = 0; i < _turtles.Count; i++)
            {
                _turtles[i].XYCoordinates = new XYCoordinates((_table.XLength / 2) - (_turtles.Count / 2) + i, _table.YLength / 2);
            }
        }

        virtual public void GameEnd()
        {
            throw new NotImplementedException();
        }

        virtual public void DoTic()
        {
            foreach (Turtle turtle in _turtles)
            {
                turtle.Move();
            }
        }
    }
}