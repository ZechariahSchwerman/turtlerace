namespace TurtleRace
{
    public enum Direction
    {
        North,
        East,
        South,
        West
    }
}