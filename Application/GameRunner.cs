/*
Trying for basic no user interaction game first
 */

namespace TurtleRace
{
    public class GameRunner
    {
        readonly private Game _game;

        public GameRunner(Game game)
        {
            _game = game;
        }

        public void Play()
        {
            _game.GameStart();
            //Some kinda renderer class that has references to same table and turtle list
            //that will be injected into game
            //render here

            while (!_game.IsGameOver())
            {
                _game.DoTic();
                //Some kinda renderer class that has references to same table and turtle list
                //that will be injected into game
                //render here
            }

            _game.GameEnd();
        }
    }
}