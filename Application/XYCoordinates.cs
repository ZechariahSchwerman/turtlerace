using System;

namespace TurtleRace
{
    public class XYCoordinates : IComparable<XYCoordinates>, IComparable
    {
        readonly public int XCoordinate;

        readonly public int YCoordinate;

        public XYCoordinates(int xCoordinate, int yCoordinate)
        {
            XCoordinate = xCoordinate;
            YCoordinate = yCoordinate;
        }
        public static XYCoordinates NewCoordsChanged(XYCoordinates xyCoordinates, Direction direction)
        {
            int tempXCoordinate = xyCoordinates.XCoordinate;
            int tempYCoordinate = xyCoordinates.YCoordinate;

            switch (direction)
            {
                case Direction.North:
                    tempYCoordinate = xyCoordinates.YCoordinate + 1;
                    break;
                case Direction.East:
                    tempXCoordinate = xyCoordinates.XCoordinate + 1;
                    break;
                case Direction.South:
                    tempYCoordinate = xyCoordinates.YCoordinate - 1;
                    break;
                //wicka wicka wild wild west...
                //if you got that reference, I pity you
                case Direction.West:
                    tempXCoordinate = xyCoordinates.XCoordinate - 1;
                    break;
            }

            return new XYCoordinates(tempXCoordinate, tempYCoordinate);

        }

        public int override bool Equals(object obj)
        {

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }



        }

    }
}