
namespace TurtleRace
{
    public struct Table
    {
        readonly public int XLength;
        readonly public int YLength;

        public Table(int x, int y)
        {
            XLength = x;
            YLength = y;
        }
    }
}
