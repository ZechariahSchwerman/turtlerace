using System;
using System.Collections.Generic;

namespace TurtleRace
{
    public class Turtle
    {
        virtual public int XPosition { get { return XYCoordinates.XCoordinate; } }

        virtual public int YPosition { get { return XYCoordinates.YCoordinate; } }

        public XYCoordinates XYCoordinates;

        readonly private Random _random;

        public Turtle()
        {

        }

        public Turtle(XYCoordinates xyCoordinates)
        {
            XYCoordinates = xyCoordinates;
        }

        public Turtle(XYCoordinates xyCoordinates, Random random)
        {
            XYCoordinates = xyCoordinates;
            _random = random;
        }

        virtual public void Move()
        {
            throw new NotImplementedException();
        }

        virtual public void Move(IEnumerable<Turtle> turtles)
        {
            int nextMove = _random.Next(4);

            var possibleXYCoords = XYCoordinates.NewCoordsChanged(XYCoordinates, (Direction)nextMove);

            while ()
            {

            }

            XYCoordinates = XYCoordinates.NewCoordsChanged(XYCoordinates, (Direction)nextMove);

        }


    }
}